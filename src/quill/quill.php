<?php
/**
 * Quill Plugin
 *
 * @author Irena Romendik <irena@voidit.net>
 * @version 1.0.0
 */

Pages::$methods['quill'] = function($pages, $params = array()) {

    // set all default values
    $defaults = array(
        'theme'   => 'snow',
        'el'      => '.editor',
        'snippet' => false,
    );

    // merge them with the user input
    $options = array_merge($defaults, $params);

    $html = '';

    // custom snippet
    if($options['snippet']) {
        $html .= snippet($options['snippet'], $options, true);
    } else {
        $html .= tpl::load(__DIR__ . DS . 'template.php', $options);
    }

    return $html;

};