<!-- Main Quill library -->
<script src="//cdn.quilljs.com/1.2.0/quill.js"></script>
<script src="//cdn.quilljs.com/1.2.0/quill.min.js"></script>

<!-- Theme included stylesheets -->
<link href="//cdn.quilljs.com/1.2.0/quill.<?php echo $options['theme'] ?>.css" rel="stylesheet">
<link href="//cdn.quilljs.com/1.2.0/quill.bubble.css" rel="stylesheet">

<!-- Core build with no theme, formatting, non-essential modules -->
<link href="//cdn.quilljs.com/1.2.0/quill.core.css" rel="stylesheet">
<script src="//cdn.quilljs.com/1.2.0/quill.core.js"></script>

<script type="text/javascript">
    var quill = new Quill(<?php echo $options['el'] ?>, {
        modules: { toolbar: true },
        theme: <?php echo $options['theme'] ?>
    });
</script>