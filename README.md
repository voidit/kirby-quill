# Quill editor for Kirby CMS #

* [Quill](http://quilljs.com) meets [Kirby](https://getkirby.com)
* Version 1.0.0-dev

## Installation

Put the `feed` folder in `/site/plugins`.

## How to use it

You can use this in a template for a textfield or in a template controller.

## Example usage

```php
<?php

echo page('quill')->quill(array(
  'theme' => 'white',
  'el'    => '#texteditor'
));
```

Check out the $defaults array in quill.php for more options.

* Alternatively, with kirby CLI 

`kirby plugin:install quill`

* Configuration

If you want to use Quill only for panel, just enable it in your site options


## For developers

```
mkdir -p my-project && cd $_
git clone --recursive -j8 git@bitbucket.org:voidit/kirby-quill.git .
git submodule update --init --recursive
npm i --save-dev
```

`npm run build:dev`

`npm run build:prod`

* How to run tests

`npm test`

### Contribution guidelines ###

* Writing tests:

`npm test`

* Code review

All PRs are encouraged and welcome

### Who do I talk to? ###

* Irena Romendik @voidit